# DMG 

##### 视频来源

- [https://www.youtube.com/watch?v=EniblIsf8tU](https://www.youtube.com/watch?v=EniblIsf8tU)



# DMG制作

- 打包macos项目

```
flutter build macos
cd /Users/taoshumin_vendor/go/src/swiftui.flutter.io/gridstack/build/macos/Build/Products/Release
ll gridstack.app
```


- 安装npm

```
╰─± npm --version
8.15.0
```

- 安装node 

```
╰─○ node --version
v18.8.0
```

- 安装 appdmg  [https://www.npmjs.com/package/appdmg](https://www.npmjs.com/package/appdmg)


安装: `npm install -g appdmg`

```
╰─± appdmg --version
node-appdmg v0.6.4

╰─± npx appdmg --version
node-appdmg v0.6.4
```

- 构建dmg

```
cd /dmg_creator

        配置文件       输出dmg地址
appdmg ./config.json ./flutter_tips.dmg 
```

# [icns 文件制作](https://zhuanlan.zhihu.com/p/348599140)
